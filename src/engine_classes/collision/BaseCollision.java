package engine_classes.collision;

import interfaces.Collisionable;
import interfaces.Collision;
import engine_classes.Vector2D;

public abstract class BaseCollision implements Collision {

	public static final float COLLISION_ELASTIC = 1.0f;
	public static final float COLLISION_INELASTIC = 0.0f;

	private float arrastCoeficient;

	public BaseCollision() {
		this(COLLISION_ELASTIC);
	}

	public BaseCollision(float arrastCoeficient) {
		this.arrastCoeficient = arrastCoeficient;
	}

	@Override
	public boolean makeCollision(Collisionable c1, Collisionable c2) {
		if (haveCollision(c1, c2)) {
			applyVelocity(c1, c2);
			awayObjects(c1, c2);
			return true;
		}
		return false;
	}

	public float getImpulse(Collisionable c1, Collisionable c2,
			Vector2D unitNormal) {
		Vector2D vrel = new Vector2D(c2.getVelocity(), c1.getVelocity());
		float vrelScalarNormal = vrel.scalarProduct(unitNormal);
		return -1 * (1 + this.arrastCoeficient) * vrelScalarNormal / ((1.0f / c1.getMass()) + (1.0f / c2.getMass()));
	}

	public void applyVelocity(Collisionable c1, Collisionable c2) {
		Vector2D unitNormal = getUnitNormal(c1, c2);
		float impulse = getImpulse(c1, c2, unitNormal);
		
		Vector2D normalScalarImpulseC1 = new Vector2D(unitNormal);
		Vector2D normalScalarImpulseC2 = new Vector2D(unitNormal);
		
		normalScalarImpulseC1.scalar(impulse / c1.getMass());
		normalScalarImpulseC2.scalar(impulse / c2.getMass());
		
		c1.getVelocity().sum(normalScalarImpulseC1);
		c2.getVelocity().sub(normalScalarImpulseC2);
	}

	public abstract boolean haveCollision(Collisionable c1, Collisionable c2);

	public abstract Vector2D getUnitNormal(Collisionable c1, Collisionable c2);

	public abstract float getMinShadow(Collisionable c1, Collisionable c2);

	public void awayObjects(Collisionable c1, Collisionable c2) {
		float shadow = getMinShadow(c1, c2);
		Vector2D unitNormal = getUnitNormal(c1, c2);
		float incrementC1 = 0.5f;
		float incrementC2 = 0.5f;
		
		if(c1.getMass() == Collisionable.MAXIMUM_MASS) {
			incrementC1 = 0.0f;
			incrementC2 *= 2.0f;
		}
		if(c2.getMass() == Collisionable.MAXIMUM_MASS) {
			incrementC2 = 0.0f;
			incrementC1 *= 2.0f;
		}
		
		c1.getPosition().incrementY(shadow * unitNormal.getY() * 1 * incrementC1);
		c1.getPosition().incrementX(shadow * unitNormal.getX() * 1 * incrementC1);

		c2.getPosition().incrementY(shadow * unitNormal.getY() * -1 * incrementC2);
		c2.getPosition().incrementX(shadow * unitNormal.getX() * -1 * incrementC2);
	}
	
	public float getArrastCoeficient() {
		return arrastCoeficient;
	}

	public void setArrastCoeficient(float arrastCoeficient) {
		this.arrastCoeficient = arrastCoeficient;
	}
}
