package engine_classes.collision;

import interfaces.Collisionable;
import interfaces.helper.PositionableHelper;

import java.util.ArrayList;

import engine_classes.Point2D;
import engine_classes.Vector2D;

public class PolygonCircleCollision extends PolygonCollision {
	
	private PositionableHelper positionableHelper;
	
	public PolygonCircleCollision() {
		this(COLLISION_ELASTIC);
	}

	public PolygonCircleCollision(float arrastCoeficient) {
		super(arrastCoeficient);
		this.positionableHelper = new PositionableHelper();
	}
	
	@Override
	public boolean makeCollision(Collisionable rect, Collisionable circle) {
		return super.makeCollision(rect, circle);
	}
	
	@Override
	protected ArrayList<Float> getScalarValues(Vector2D direction,
			Point2D[] points) {
		if(points.length > 2) {
			return super.getScalarValues(direction, points);
		} else {
			Point2D newPoints[] = this.positionableHelper.getCirclePoints(points, direction);
			return super.getScalarValues(direction, newPoints);
		}
	}
}
