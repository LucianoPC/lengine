package engine_classes.collision;

import interfaces.Collisionable;
import interfaces.Collision;

public class NoneCollision implements Collision {

	@Override
	public boolean makeCollision(Collisionable c1, Collisionable c2) {
		return true;
	}

}
