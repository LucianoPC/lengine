package engine_classes.collision;

import interfaces.Collisionable;
import engine_classes.Point2D;
import engine_classes.Vector2D;

public class CircleCollision extends BaseCollision {

	public CircleCollision() {
		this(COLLISION_ELASTIC);
	}

	public CircleCollision(float arrastCoeficient) {
		super(arrastCoeficient);
	}

	@Override
	public boolean haveCollision(Collisionable c1, Collisionable c2) {
		return ((c1.getWidth() / 2.0f) + (c2.getWidth() / 2.0f)) >= (c1
				.getCenter().getDistance(c2.getCenter()));
	}

	@Override
	public Vector2D getUnitNormal(Collisionable c1, Collisionable c2) {
		Point2D pointCenter1 = c1.getCenter();
		Point2D pointCenter2 = c2.getCenter();

		Vector2D unitNormal = new Vector2D(pointCenter2, pointCenter1);
		unitNormal.scalar(1.0f / unitNormal.getMagnitude());

		return unitNormal;
	}

	@Override
	public float getMinShadow(Collisionable c1, Collisionable c2) {
		return (c2.getWidth() / 2.0f) + (c2.getWidth() / 2.0f)
				- c1.getCenter().getDistance(c2.getCenter());
	}

}
