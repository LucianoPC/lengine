package engine_classes.collision;

import interfaces.Collisionable;
import engine_classes.Vector2D;

public class AABBCollision extends BaseCollision {

	public AABBCollision() {
		this(COLLISION_ELASTIC);
	}

	public AABBCollision(float arrastCoeficient) {
		super(arrastCoeficient);
	}

	@Override
	public boolean haveCollision(Collisionable c1, Collisionable c2) {
		return c1.getRect().intersect(c2.getRect());
	}

	@Override
	public Vector2D getUnitNormal(Collisionable c1, Collisionable c2) {
		float b1X = c1.getRect().left;
		float b1Y = c1.getRect().top;
		float b2X = c2.getRect().left;
		float b2Y = c2.getRect().top;

		float shadowX = getShadowX(c1, c2);
		float shadowY = getShadowY(c1, c2);

		Vector2D unitNormal = new Vector2D(0.0f, 0.0f);
		if (shadowX > shadowY) {
			unitNormal.setY(b2Y < b1Y ? 1 : -1);
		} else {
			unitNormal.setX(b2X < b1X ? 1 : -1);
		}
		return unitNormal;
	}

	@Override
	public float getMinShadow(Collisionable c1, Collisionable c2) {
		return (float)Math.min(getShadowX(c1, c2), getShadowY(c1, c2));
	}

	public float getShadowX(Collisionable c1, Collisionable c2) {
		float minOfMax = Math.min(c1.getRect().right, c2.getRect().right);
		float maxOfMin = Math.max(c1.getRect().left, c2.getRect().left);
		return minOfMax - maxOfMin;
	}

	public float getShadowY(Collisionable c1, Collisionable c2) {
		float minOfMax = Math.min(c1.getRect().bottom, c2.getRect().bottom);
		float maxOfMin = Math.max(c1.getRect().top, c2.getRect().top);
		return minOfMax - maxOfMin;
	}

}
