package engine_classes.collision;

import interfaces.Collisionable;
import interfaces.helper.PositionableHelper;

import java.util.ArrayList;
import java.util.Collections;

import engine_classes.Point2D;
import engine_classes.Vector2D;

public class RectCollision extends BaseCollision {
	
	private float minShadow;
	private Vector2D directionMinShadow;
	private PositionableHelper positionableHelper;
	
	public RectCollision() {
		this(COLLISION_ELASTIC);
	}

	public RectCollision(float arrastCoeficient) {
		super(arrastCoeficient);
		this.positionableHelper = new PositionableHelper();
	}
	
	@Override
	public boolean haveCollision(Collisionable c1, Collisionable c2) {
		Point2D pts1[] = this.positionableHelper.getSquarePoints(c1, c1.getDegree());
		Point2D pts2[] = this.positionableHelper.getSquarePoints(c2, c2.getDegree());
		
		this.minShadow = 0;
		for(int i = 0; i < pts1.length / 2; i++) {
			int next = (i + 1) % pts1.length;
			int actual = i;

			Vector2D direction = new Vector2D(pts1[actual], pts1[next]);
			direction.setXY(-direction.getY(), direction.getX());
			direction.scalar(1.0f / direction.getMagnitude());
			float shadow = getShadow(direction, pts1, pts2);
			
			if(i == 0 || shadow < this.minShadow) {
				this.minShadow = shadow;
				this.directionMinShadow = direction;
				
			}

			if(shadow < 0) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public Vector2D getUnitNormal(Collisionable c1, Collisionable c2) {
		Vector2D unitNormal = new Vector2D(this.directionMinShadow);		
		unitNormal.scalar(1.0f / unitNormal.getMagnitude());

		float scalarB1 = unitNormal.scalarProduct(new Vector2D(c1.getCenter()));
		float scalarB2 = unitNormal.scalarProduct(new Vector2D(c2.getCenter()));
		
		if(scalarB2 > scalarB1) {
			unitNormal.scalar(-1.0f);
		}
		
		return unitNormal;
	}

	@Override
	public float getMinShadow(Collisionable c1, Collisionable c2) {
		return this.minShadow;
	}
	
	protected ArrayList<Float> getScalarValues(Vector2D direction, Point2D points[]) {
		ArrayList<Float> shadowValues = new ArrayList<Float>();
		for(int i = 0; i < points.length; i++) {
			shadowValues.add(direction.scalarProduct(new Vector2D(points[i])));
		}
		Collections.sort(shadowValues);
		return shadowValues;
	}
	
	public float getShadow(Vector2D direction, Point2D pts1[], Point2D pts2[]) {
		ArrayList<Float> scalarValues1 = getScalarValues(direction, pts1);
		ArrayList<Float> scalarValues2 = getScalarValues(direction, pts2);
		
		float min1 = scalarValues1.get(0);
		float max1 = scalarValues1.get(scalarValues1.size() - 1);
		float min2 = scalarValues2.get(0);
		float max2 = scalarValues2.get(scalarValues2.size() - 1);
		
		float minOfMax = Math.min(max1, max2);
		float maxOfMin = Math.max(min1, min2);
		
		return minOfMax - maxOfMin;
	}

	public float getShadowX(Collisionable c1, Collisionable c2) {
		float minOfMax = Math.min(c1.getRect().right, c2.getRect().right);
		float maxOfMin = Math.max(c1.getRect().left, c2.getRect().left);
		return minOfMax - maxOfMin;
	}

	public float getShadowY(Collisionable c1, Collisionable c2) {
		float minOfMax = Math.min(c1.getRect().bottom, c2.getRect().bottom);
		float maxOfMin = Math.max(c1.getRect().top, c2.getRect().top);
		return minOfMax - maxOfMin;
	}

}
