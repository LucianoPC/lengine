package engine_classes.collision;

import interfaces.Collisionable;
import interfaces.Collision;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PhysicCollision implements Collision {
	
	public enum OrderBy implements Comparator<Collisionable> {
		NUM_POINTS {
			@Override
			public int compare(Collisionable a, Collisionable b) {
				return Float.compare(a.getPoints().length, b.getPoints().length);
			}
		}
	}

	private CircleCollision circleCollision;
	private PolygonCircleCollision polygonCircleCollision;
	
	public PhysicCollision() {
		this(BaseCollision.COLLISION_ELASTIC);
	}
	
	public PhysicCollision(float arrastCoeficient) {
		this.circleCollision = new CircleCollision(arrastCoeficient);
		this.polygonCircleCollision = new PolygonCircleCollision(arrastCoeficient);
	}

	@Override
	public boolean makeCollision(Collisionable c1, Collisionable c2) {
		ArrayList<Collisionable> collisionableList = new ArrayList<Collisionable>();
		collisionableList.add(c1);
		collisionableList.add(c2);
		Collections.sort(collisionableList, OrderBy.NUM_POINTS);
		c1 = collisionableList.get(0);
		c2 = collisionableList.get(1);
		if(c2.getPoints().length > 2) {
			return this.polygonCircleCollision.makeCollision(c2, c1);
		} else {
			return this.circleCollision.makeCollision(c1, c2);
		}
	}


}
