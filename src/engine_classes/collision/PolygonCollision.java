package engine_classes.collision;

import interfaces.Collisionable;

import java.util.ArrayList;
import java.util.Collections;

import engine_classes.Point2D;
import engine_classes.Vector2D;

public class PolygonCollision extends BaseCollision {

	private float minShadow;
	private Vector2D directionMinShadow;
	
	public PolygonCollision() {
		this(COLLISION_ELASTIC);
	}

	public PolygonCollision(float arrastCoeficient) {
		super(arrastCoeficient);
	}
	
	@Override
	public boolean haveCollision(Collisionable c1, Collisionable c2) {
		Point2D pts1[] = c1.getPoints();
		Point2D pts2[] = c2.getPoints();
		
		this.minShadow = 0;
		int length = (pts1.length % 2 == 0) ? (pts1.length / 2) : pts1.length;
		for(int i = 0; i < length; i++) {
			int next = (i + 1) % pts1.length;
			int actual = i;

			Vector2D direction = new Vector2D(pts1[actual], pts1[next]);
			direction.setXY(-direction.getY(), direction.getX());
			direction.scalar(1.0f / direction.getMagnitude());
			float shadow = getShadow(direction, pts1, pts2);
			
			if(i == 0 || shadow < this.minShadow) {
				this.minShadow = shadow;
				this.directionMinShadow = direction;
				
			}

			if(shadow < 0) {
				return false;
			}
		}
		
		return true;
	}

	@Override
	public Vector2D getUnitNormal(Collisionable c1, Collisionable c2) {
		Vector2D unitNormal = new Vector2D(this.directionMinShadow);		
		unitNormal.scalar(1.0f / unitNormal.getMagnitude());

		float scalarB1 = unitNormal.scalarProduct(new Vector2D(c1.getCenter()));
		float scalarB2 = unitNormal.scalarProduct(new Vector2D(c2.getCenter()));
		
		if(scalarB2 > scalarB1) {
			unitNormal.scalar(-1.0f);
		}
		
		return unitNormal;
	}

	@Override
	public float getMinShadow(Collisionable c1, Collisionable c2) {
		return this.minShadow;
	}
	
	protected ArrayList<Float> getScalarValues(Vector2D direction, Point2D points[]) {
		ArrayList<Float> shadowValues = new ArrayList<Float>();
		for(int i = 0; i < points.length; i++) {
			shadowValues.add(direction.scalarProduct(new Vector2D(points[i])));
		}
		Collections.sort(shadowValues);
		return shadowValues;
	}
	
	public float getShadow(Vector2D direction, Point2D pts1[], Point2D pts2[]) {
		ArrayList<Float> scalarValues1 = getScalarValues(direction, pts1);
		ArrayList<Float> scalarValues2 = getScalarValues(direction, pts2);
		
		float min1 = scalarValues1.get(0);
		float max1 = scalarValues1.get(scalarValues1.size() - 1);
		float min2 = scalarValues2.get(0);
		float max2 = scalarValues2.get(scalarValues2.size() - 1);
		
		float minOfMax = Math.min(max1, max2);
		float maxOfMin = Math.max(min1, min2);
		
		return minOfMax - maxOfMin;
	}
	
}
