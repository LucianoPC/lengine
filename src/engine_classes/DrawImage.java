package engine_classes;

import util.Screen;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class DrawImage {

	private Bitmap bitmap;
	private Paint paint;
	
	public DrawImage(Resources resources, int id) {
		this.paint = new Paint();
		this.bitmap = BitmapFactory.decodeResource(resources, id);
	}
	
	public void draw(Canvas canvas, float x, float y) {
		canvas.drawBitmap(this.bitmap, x, Screen.width - y, this.paint);
	}
	
	public void draw(Canvas canvas, Rect source, RectF destination) {
		draw(canvas, source, destination, 0);
	}
	
	public void draw(Canvas canvas, Rect source, RectF destination, int degrees) {;
		canvas.save();
		canvas.rotate(360 - degrees, destination.centerX(), destination.centerY());
		canvas.drawBitmap(this.bitmap, source, destination, this.paint);
		canvas.restore();
	}
	
	public Paint getPaint() {
		return this.paint;
	}
	
	public int getWidth() {
		return this.bitmap.getWidth();
	}
	
	public int getHeight() {
		return this.bitmap.getHeight();
	}
	
}
