package engine_classes;

import interfaces.Touchable;
import android.graphics.Canvas;
import android.view.MotionEvent;

public class TouchArea implements Touchable {

	private Touch touch;
	private Point2D point2d;
	private int width;
	private int height;
	
	private boolean enable;
	private boolean enableCallListener;
	
	private boolean touchedDown;
	
	public TouchArea(int width, int height) {
		this.width = width;
		this.height = height;
		
		this.touch = new Touch(this);
		this.point2d = new Point2D();
		
		this.enable = true;
		this.enableCallListener = true;
		this.touchedDown = false;
	}

	@Override
	public void draw(Canvas canvas) {
		
	}

	@Override
	public Point2D getPosition() {
		return this.point2d;
	}

	@Override
	public int getWidth() {
		return this.width;
	}
	
	public void setWidth(int width) {
		this.width = width;
	}

	@Override
	public int getHeight() {
		return this.height;
	}
	
	public void setHeight(int height) {
		this.height = height;
	}

	@Override
	public Touch getTouch() {
		return this.touch;
	}

	@Override
	public boolean isMyMotionEvent(MotionEvent event) {
		if(this.enable &&
				!this.touchedDown &&
				(event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) &&
				intersect(event)) {
			this.touchedDown = true;
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP) {
			this.touchedDown = false;
		}
		
		return false;
	}

	@Override
	public boolean isEnableCallListener() {
		return this.enableCallListener;
	}
	
	public void setCallListener(boolean callListener) {
		this.enableCallListener = callListener;
	}
	
	
	
	protected boolean intersect(MotionEvent event) {
		return (this.getPosition().getX() <= event.getX()) &&
			   (this.getPosition().getX() + this.getWidth() >= event.getX()) &&
			   (this.getPosition().getY() <= event.getY()) &&
			   (this.getPosition().getY() + this.getHeight() >= event.getY());
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	@Override
	public float getDegree() {
		return 0;
	}
	
}
