package engine_classes;

import interfaces.Touchable;
import android.content.res.Resources;
import android.view.MotionEvent;

public class Button extends Image implements Touchable{

	private Touch touch;
	private boolean enable;
	private boolean enableCallListener;
	private boolean haveLastMotionEvent;

	public Button(Resources resources, int id, int spriteRow, int spriteCol, float x, float y) {
		super(resources, id, spriteRow, spriteCol, x, y);

		this.touch = new Touch(this);
		this.enable = true;
		this.enableCallListener = true;
		this.haveLastMotionEvent = false;
	}
	
	public Button(Resources resources, int id) {
		this(resources, id, 1, 1, 0.f, 0.f);
	}
	
	public Button(Resources resources, int id, int spriteRow, int spriteCol) {
		this(resources, id, spriteRow, spriteCol, 0.f, 0.f);
	}
	
	@Override
	public Touch getTouch() {
		return this.touch;
	}

	@Override
	public boolean isMyMotionEvent(MotionEvent event) {
		if(!this.enable) {
			return false;
		}
		
		this.enableCallListener = false;
		
		if(event.getAction() == MotionEvent.ACTION_DOWN &&
				intersect(event)) {
			this.haveLastMotionEvent = true;
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE &&
				!intersect(event)) {
			this.haveLastMotionEvent = false;
			return false;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE &&
				intersect(event)) {
			return this.haveLastMotionEvent;
		} else if(event.getAction() == MotionEvent.ACTION_UP &&
				this.haveLastMotionEvent) {
			this.haveLastMotionEvent = false;
			this.enableCallListener = true;
			return true;
		}
		
		this.haveLastMotionEvent = false;		
		return false;
	}
	
	private boolean intersect(MotionEvent event) {
		float fakeArea = this.getFakeArea();
		return ( (this.getPosition().getX() - fakeArea) <= event.getX()) &&
			   ( (this.getPosition().getX() + this.getWidth() + fakeArea) >= event.getX()) &&
			   ( (this.getPosition().getY() - fakeArea)  <= event.getY()) &&
			   ( (this.getPosition().getY() + this.getHeight() + fakeArea) >= event.getY());
	}

	@Override
	public boolean isEnableCallListener() {
		return this.enableCallListener;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
}
