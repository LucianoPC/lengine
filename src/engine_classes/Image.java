package engine_classes;

import interfaces.Drawable;
import interfaces.Paintable;
import interfaces.Positionable;
import util.Screen;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import engine_classes.Point2D.ValueListener;

public class Image implements Drawable, Positionable, Paintable, ValueListener {
	
	private float fakeArea;
	private Point2D point2d;
	private DrawImage drawImage;
	private boolean visible;
	private boolean imageMoved;
	private boolean imageChangeSprite;
	private boolean inScreen;
	
	private int width, height;
	private int incrementWidth, incrementHeight;
	
	private int degree;

	private Rect origin;
	private RectF destination;
	private int spriteRows, spriteCols, actualRow, actualCol;
	
	public Image(Resources resources, int id, int spriteRows, int spriteCols, float x, float y) {
		this.drawImage = new DrawImage(resources, id);
		this.point2d = new Point2D(x, y);
		this.point2d.setValueListener(this);
		this.origin = new Rect();
		this.destination = new RectF();
		
		this.spriteRows = spriteRows;
		this.spriteCols = spriteCols;

		this.width = this.drawImage.getWidth() / this.spriteCols;
		this.height = this.drawImage.getHeight() / this.spriteRows;
		
		if( (Screen.width / 320.0f) < (Screen.height / 480.0f) ) {
			this.incrementWidth = (int)(this.width * (Screen.width / 320.0f));
			this.incrementHeight = (int)(this.height * (Screen.width / 320.0f));
		} else {
			this.incrementWidth = (int)(this.width * (Screen.height / 480.0f));
			this.incrementHeight = (int)(this.height * (Screen.height / 480.0f));
		}
		
		this.incrementWidth /= Screen.density;
		this.incrementHeight /= Screen.density;

		this.visible = true;
		
		this.fakeArea = 0;
		this.actualRow = 0;
		this.actualCol = 0;
		this.degree = 0;
		
		this.imageMoved = true;
		this.imageChangeSprite = true;
		this.inScreen = false;
	}
	
	public Image(Resources resources, int id, int spriteRows, int spriteCols) {
		this(resources, id, spriteRows, spriteCols, 0, 0);
	}
	
	public Image(Resources resources, int id, float x, float y) {
		this(resources, id, 1, 1, x, y);
	}
	
	public Image(Resources resources, int id) {
		this(resources, id, 1, 1, 0, 0);
	}

	@Override
	public void draw(Canvas canvas) {
		if(this.visible) {
			if(this.imageMoved) {
				this.imageMoved = false;
				this.updateDestinationRectangle();
				this.inScreen = imageInScreen();
			}
			if(this.imageChangeSprite) {
				this.imageChangeSprite = false;
				this.updateOriginRectangle();
			}
			if(this.inScreen) {
				this.drawImage.draw(canvas, this.origin, this.destination, this.degree);
			}
		}
	}
	
	@Override
	public Point2D getPosition() {
		return this.point2d;
	}

	@Override
	public int getWidth() {
		return this.incrementWidth;
	}

	@Override
	public int getHeight() {
		return this.incrementHeight;
	}

	@Override
	public Paint getPaint() {
		return this.drawImage.getPaint();
	}

	public float getFakeArea() {
		return fakeArea;
	}

	public void setFakeArea(float fakeArea) {
		this.fakeArea = fakeArea;
	}
	
	public void setVisible(boolean visible) {
		this.visible = visible;
	}
	
	public boolean isVisible() {
		return this.visible;
	}
	
	public void incrementRow() {
		this.actualRow = (this.actualRow + 1) % this.spriteRows;
		this.imageChangeSprite = true;
	}
	
	public void dencrementRow() {
		this.actualRow = (this.actualRow - 1) % this.spriteRows;
		this.imageChangeSprite = true;
	}
	
	public void incrementCols() {
		this.actualCol = (this.actualCol + 1) % this.spriteCols;
		this.imageChangeSprite = true;
	}
	
	public void dencrementCols() {
		this.actualCol = (this.actualCol - 1) % this.spriteCols;
		this.imageChangeSprite = true;
	}

	public int getActualRow() {
		return actualRow;
	}

	public void setActualRow(int actualRow) {
		this.actualRow = actualRow;
		this.imageChangeSprite = true;
	}

	public int getActualCol() {
		return actualCol;
	}

	public void setActualCol(int actualCol) {
		this.actualCol = actualCol;
		this.imageChangeSprite = true;
	}

	public int getSpriteRows() {
		return spriteRows;
	}

	public int getSpriteCols() {
		return spriteCols;
	}
	
	public float getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}
	
	private void updateOriginRectangle() {
		int spriteBeginX = actualCol * this.width;
		int spriteEndX = spriteBeginX + this.width;
		
		int spriteBeginY = actualRow * this.height;
		int spriteEndY = spriteBeginY + this.height;
		
		this.origin.set(spriteBeginX, spriteBeginY, spriteEndX, spriteEndY);
	}
	
	private void updateDestinationRectangle() {
		this.destination.set(this.getPosition().getX(), this.getPosition().getY(),
				this.getPosition().getX() + this.incrementWidth,
				this.getPosition().getY() + this.incrementHeight);
	}

	@Override
	public void onValueChanged(Point2D point2d) {
		this.imageMoved = true;
	}
	
	private boolean imageInScreen() {
		return true;
//		float x = this.getPosition().getX();
//		float y = this.getPosition().getY();
//		
//		return x + getWidth() > 0 && x < Screen.width &&
//				y + getHeight() > 0 && y < Screen.height;
	}
}
