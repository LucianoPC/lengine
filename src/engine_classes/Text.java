package engine_classes;

import interfaces.Drawable;
import interfaces.Paintable;
import interfaces.Positionable;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Rect;
import android.graphics.Typeface;

public class Text implements Drawable, Positionable, Paintable {

	private String text;
	
	private Paint paint;
	private Point2D point2d;
	
	public Text(String text, int textSize, int color,
			    Align align, float x, float y) {
		this.text = text;
		
		this.paint = new Paint();
		this.point2d = new Point2D(x, y);
		
		this.paint.setColor(color);
		this.paint.setTextSize(textSize);
		this.paint.setTextAlign(align);
	}
	
	public Text(String text, int textSize, int color, Align align) {
		this(text, textSize, color, align, 0, 0);
	}
	
	public Text(String text, int textSize, int color) {
		this(text, textSize, color, Align.LEFT);
	}
	
	public Text(String text, int textSize) {
		this(text, textSize, Color.BLACK);
	}
	
	public Text(String text, int textSize, int color, String font_path, Context context){
		this(text, textSize, color);
		this.paint.setTypeface(Typeface.createFromAsset(context.getAssets(), font_path));
	}

	@Override
	public int getWidth() {
		return (int) this.getPaint().measureText(text);
	}

	@Override
	public int getHeight() {
		Rect bounds = new Rect();
		this.getPaint().getTextBounds(this.text, 0, this.text.length(), bounds);
		return bounds.height();
	}
	
	public String getText() {
		return this.text;
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public int getSize() {
		return (int) this.getPaint().getTextSize();
	}
	
	public void setSize(int textSize) {
		this.getPaint().setTextSize(textSize);
	}
	
	public int getColor() {
		return this.getPaint().getColor();
	}
	
	public void setColor(int color) {
		this.getPaint().setColor(color);
	}
	
	public Align getAlign() {
		return this.getPaint().getTextAlign();
	}
	
	public void setAlign(Align align) {
		this.getPaint().setTextAlign(align);
	}

	@Override
	public Point2D getPosition() {
		return this.point2d;
	}

	@Override
	public void draw(Canvas canvas) {
		canvas.drawText(this.text, this.point2d.getX(), this.point2d.getY() + this.getHeight(), 
		        this.getPaint());
	}

	@Override
	public Paint getPaint() {
		return this.paint;
	}

	@Override
	public float getDegree() {
		return 0;
	}

}
