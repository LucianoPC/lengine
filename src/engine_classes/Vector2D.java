package engine_classes;

public class Vector2D extends Point2D {
	
	public Vector2D(Point2D point2d) {
		super(point2d);
	}
	
	public Vector2D(float x, float y) {
		super(x, y);
	}
	
	public Vector2D(Point2D point2dHead, Point2D point2dTail) {
		this(point2dTail.getX() - point2dHead.getX(), point2dTail.getY() - point2dHead.getY());		
	}
	
	public Vector2D() {
		this(0, 0);
	}
	
	public void scalar(float value) {
		scalarX(value);
		scalarY(value);
	}

	public void scalarX(float value) {
		this.x *= value;
	}
	
	public void scalarY(float value) {
		this.y *= value;
	}
	
	public float scalarProduct(Vector2D vector2d) {
		return (this.x * vector2d.x) + (this.y * vector2d.y);
	}
	
	public float getMagnitude() {
		float magnitude = (this.getX() * this.getX()) + (this.getY() * this.getY());
		magnitude = (float) Math.sqrt(magnitude);
		return magnitude;
	}
	
	public Vector2D getUnitVector() {
		Vector2D unitVector = new Vector2D(this);
		unitVector.scalar(1.0f / unitVector.getMagnitude());
		return unitVector;
	}
	
	public void rotate(float angle) {
		float radian = (float)Math.PI * angle / 180.0f;
		
		this.x = ((float)Math.cos(radian) * this.x) - ((float)Math.sin(radian) * this.y);
		this.y = ((float)Math.sin(radian) * this.x) + ((float)Math.cos(radian) * this.y);
	}
	
	public void changeDirection() {
		this.y *= -1;
	}
	
	public void changeSense() {
		this.x *= -1;
	}
	
}
