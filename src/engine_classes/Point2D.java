package engine_classes;

import java.util.Comparator;


public class Point2D {

	private static final double MULTIPLY = 1000.0;
	
	public interface ValueListener {
		public void onValueChanged(Point2D point2d);
	}
	
	public enum OrderBy implements Comparator<Point2D> {
		X {
			@Override
			public int compare(Point2D lhs, Point2D rhs) {
				return lhs.getX().compareTo(rhs.getX());
			}
		},
		Y {
			@Override
			public int compare(Point2D lhs, Point2D rhs) {
				return lhs.getY().compareTo(rhs.getY());
			}
		}
		
	}

	protected Float x;
	protected Float y;

	private ValueListener valueListener;

	public Point2D(Point2D point2d) {
		this(point2d.x, point2d.y);
	}

	public Point2D(float x, float y) {
		this.x = x;
		this.y = y;

		this.valueListener = null;
	}

	public Point2D() {
		this(0, 0);
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof Point2D)) {
			return false;
		}

		Point2D otherPoint2D = (Point2D) object;
		return (this.x.equals(otherPoint2D.getX()))
				&& (this.y.equals(otherPoint2D.getY()));
	}

	@Override
	public String toString() {
		return "Point2D(" + this.x + ", " + this.y + ")";
	}
	
	public void rotate(float degree) {
		rotate(degree, new Point2D(0.0f, 0.0f));
	}

	public void rotate(float degree, Point2D origin) {
		double cos = Math.cos(degree * Math.PI / 180.0) * MULTIPLY;
		double sin = Math.sin(degree * Math.PI / 180.0) * MULTIPLY;

		this.sub(origin);
		float x = (float)(( (cos * this.x) + (sin * this.y)) / MULTIPLY);
		float y = (float)((-(sin * this.x) + (cos * this.y)) / MULTIPLY);
		this.setX(x);
		this.setY(y);
		this.sum(origin);
	}
	
	public float getDistance(Point2D point2d) {
		float xp1 = this.getX();
		float yp1 = this.getY();
		float xp2 = point2d.getX();
		float yp2 = point2d.getY();

		return (float) Math.sqrt(((xp1 - xp2) * (xp1 - xp2))
				+ ((yp1 - yp2) * (yp1 - yp2)));
	}

	public void set(Point2D point2D) {
		this.x = point2D.x;
		this.y = point2D.y;
	}

	public void sum(Point2D point2D) {
		this.x += point2D.x;
		this.y += point2D.y;
	}

	public void sub(Point2D point2D) {
		this.x -= point2D.x;
		this.y -= point2D.y;
	}

	public void middlePosition(Point2D p1, Point2D p2) {
		this.x = (p1.getX() + p2.getX()) / 2;
		this.y = (p1.getY() + p2.getY()) / 2;
		notifyOnPositionChanged();
	}

	public void setXY(float x, float y) {
		this.x = x;
		this.y = y;
		notifyOnPositionChanged();
	}

	public void incrementPosition(float x, float y) {
		this.x += x;
		this.y += y;
		notifyOnPositionChanged();
	}

	public Float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
		notifyOnPositionChanged();
	}

	public void incrementX(float x) {
		this.x += x;
		notifyOnPositionChanged();
	}

	public Float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
		notifyOnPositionChanged();
	}

	public void incrementY(float y) {
		this.y += y;
		notifyOnPositionChanged();
	}

	private void notifyOnPositionChanged() {
		if (this.valueListener != null) {
			this.valueListener.onValueChanged(this);
		}
	}

	public void setValueListener(ValueListener point2dListener) {
		this.valueListener = point2dListener;
	}

	public static Point2D sum(Point2D a, Point2D b) {
		Point2D result = new Point2D();
		result.x = a.x + b.x;
		result.y = a.y + b.y;
		return result;
	}

	public static Point2D sub(Point2D a, Point2D b) {
		Point2D result = new Point2D();
		result.x = a.x - b.x;
		result.y = a.y - b.y;
		return result;
	}
}
