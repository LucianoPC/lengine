package engine_classes;

import interfaces.Touchable;
import android.view.MotionEvent;

public class Touch {

	public interface TouchListener {
		public void onTouchEvent(Touchable touchable, MotionEvent event);
	}

	private Touchable touchable;
	private TouchListener touchlistener;
	
	public Touch(Touchable touchable) {
		this.touchable = touchable;
	}
	
	public boolean isMyMotionEvent(MotionEvent event) {
		if(this.touchable.isMyMotionEvent(event)) {
			notifyListenerOnTouchEvent(event);
			return true;
		}
		return false;
	}
	
	public TouchListener getTouchlistener() {
		return touchlistener;
	}
	
	public void setTouchlistener(TouchListener touchlistener) {
		this.touchlistener = touchlistener;
	}
	
	private void notifyListenerOnTouchEvent(MotionEvent event) {
		if(this.touchlistener != null && this.touchable.isEnableCallListener()) {
			this.touchlistener.onTouchEvent(this.touchable, event);
		}
	}
	
}
