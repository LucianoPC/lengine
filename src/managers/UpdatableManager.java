package managers;

import interfaces.GameObject;
import interfaces.Updatable;

public class UpdatableManager extends Manager<Updatable> {

	public UpdatableManager() {
		
	}

	@Override
	public void updateManager() {
		for(int i = 0; i < this.gameObjectlist.size(); i++) {
			this.gameObjectlist.get(i).update();
		}
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Updatable;
	}

	@Override
	protected Updatable getMyInstance(GameObject gameObject) {
		return (Updatable) gameObject;
	}
}
