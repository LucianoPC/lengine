package managers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import interfaces.GameObject;
import interfaces.Updatable;

public abstract class Manager<O> implements Updatable {

	private long stopedTime;
	private long initStopTime;
	
	private boolean enable;
	
	protected ArrayList<O> gameObjectlist;
	protected List<O> safeGameObjectList;
	
	public Manager() {
		this.enable = true;
		this.stopedTime = 0;
		this.initStopTime = 0;
		this.gameObjectlist = new ArrayList<O>();
		this.safeGameObjectList = Collections.unmodifiableList(this.gameObjectlist);
	}
	
	public void addGameObject(GameObject gameObject) {
		if(isMyInstance(gameObject)) {
			O object = getMyInstance(gameObject);
			this.gameObjectlist.add(object);
		}
	}
	
	public void removeGameObject(GameObject gameObject) {
		if(isMyInstance(gameObject)) {
			O object = getMyInstance(gameObject);
			this.gameObjectlist.remove(object);
		}
	}
	
	@Override
	public void update() {
		if(this.enable) {
			updateManager();
		}
	}
	
	public List<O> getGameObjectList() {
		return this.safeGameObjectList;
	}
	
	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		if(enable == false) {
			this.initStopTime = System.currentTimeMillis();
		} else {
			this.stopedTime += System.currentTimeMillis() - this.initStopTime;
			this.initStopTime = System.currentTimeMillis();
		}
		this.enable = enable;
	}

	public long getStopedTimeMillis() {
		return stopedTime;
	}

	protected abstract void updateManager();
	protected abstract boolean isMyInstance(GameObject gameObject);
	protected abstract O getMyInstance(GameObject gameObject);
}
