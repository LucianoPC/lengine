package managers;

import interfaces.GameObject;
import interfaces.Steerable;
import engine_classes.Point2D;
import engine_classes.Vector2D;


public class SteerableManager extends Manager<Steerable> {

	private long actualTime;
	private long deltaTime;
	private long lastTime;
	
	public SteerableManager() {
		this.lastTime = System.currentTimeMillis();
	}
	
	@Override
	public void updateManager() {
		this.actualTime = System.currentTimeMillis() - this.getStopedTimeMillis();
		this.deltaTime = (this.actualTime - this.lastTime);
		
		Steerable steerable;
		Point2D point2d;
		Vector2D vector2d;
		
		for(int i = 0; i < this.gameObjectlist.size(); i++) {
			steerable = this.gameObjectlist.get(i);
			
			if(steerable.isMoving()) {
				point2d = steerable.getPosition();
				vector2d = steerable.getVelocity();
				
				point2d.incrementX(vector2d.getX() * deltaTime * 0.001f);
				point2d.incrementY(vector2d.getY() * deltaTime * 0.001f);
			}
		}
		
		this.lastTime = this.actualTime;
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Steerable;
	}

	@Override
	protected Steerable getMyInstance(GameObject gameObject) {
		return (Steerable) gameObject;
	}
}
