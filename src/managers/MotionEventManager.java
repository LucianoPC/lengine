package managers;

import interfaces.GameObject;
import interfaces.Touchable;
import mainloop.MotionEventQueue;

public class MotionEventManager extends Manager<Touchable> {

	private MotionEventQueue motionEventQueue;
	
	public MotionEventManager() {
		this.motionEventQueue = MotionEventQueue.getInstance();
	}

	@Override
	public void updateManager() {
		while(!this.motionEventQueue.isEmpty()) {
			for(int i = this.gameObjectlist.size()-1; i >= 0; i--) {
				if(this.gameObjectlist.get(i).getTouch().isMyMotionEvent(this.motionEventQueue.getFirst())) {
					break;
				}
			}
			this.motionEventQueue.removeFirst();
		}
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Touchable;
	}

	@Override
	protected Touchable getMyInstance(GameObject gameObject) {
		return (Touchable) gameObject;
	}
}
