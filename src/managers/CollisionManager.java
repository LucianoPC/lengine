package managers;

import interfaces.Collisionable;
import interfaces.GameObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class CollisionManager extends Manager<Collisionable> {

	public enum OrderBy implements Comparator<Collisionable> {
		X {
			@Override
			public int compare(Collisionable a, Collisionable b) {
				return Float.compare(a.getRect().left, b.getRect().left);
			}
		}
	}

	public interface CollisionListener {
		public boolean onCollisionOcurred(Collisionable c1, Collisionable c2);
	}

	private CollisionListener collisionListener;
	private ArrayList<Collisionable> collisionC1;
	private ArrayList<Collisionable> collisionC2;

	public CollisionManager() {
		this.collisionC1 = new ArrayList<Collisionable>();
		this.collisionC2 = new ArrayList<Collisionable>();
	}

	@Override
	public void updateManager() {
		this.collisionC1.clear();
		this.collisionC2.clear();

		Collections.sort(this.gameObjectlist, OrderBy.X);
		for (int i = 0; i < this.gameObjectlist.size(); i++) {
			Collisionable c1 = this.gameObjectlist.get(i);
			for (int j = i + 1; j < this.gameObjectlist.size(); j++) {
				Collisionable c2 = this.gameObjectlist.get(j);
				if (c2.getRect().left > c1.getRect().right) {
					break;
				}
				if ((!c1.isMoving()) && (!c2.isMoving())) {
					continue;
				}
				if (c1.getRect().intersect(c2.getRect())) {
					this.collisionC1.add(c1);
					this.collisionC2.add(c2);
				}
			}
		}

		for (int i = 0; i < this.collisionC1.size(); i++) {
			if(!notifyListener(this.collisionC1.get(i), this.collisionC2.get(i))) {
				notifyListener(this.collisionC2.get(i), this.collisionC1.get(i));
			}
		}
	}

	private boolean notifyListener(Collisionable c1, Collisionable c2) {
		if (this.collisionListener != null) {
			return this.collisionListener.onCollisionOcurred(c1, c2);
		}
		return false;
	}

	public void setCollisionListener(CollisionListener collisionListener) {
		this.collisionListener = collisionListener;
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Collisionable;
	}

	@Override
	protected Collisionable getMyInstance(GameObject gameObject) {
		return (Collisionable) gameObject;
	}

}
