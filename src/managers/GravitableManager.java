package managers;

import interfaces.GameObject;
import interfaces.Gravitable;
import engine_classes.Vector2D;

public class GravitableManager extends Manager<Gravitable> {

	private long actualTime;
	private long deltaTime;
	private long lastTime;
	
	private Vector2D vector2d;
	
	public GravitableManager() {
		this.vector2d = new Vector2D();
		this.lastTime = System.currentTimeMillis();
	}
	
	@Override
	public void updateManager() {
		this.actualTime = System.currentTimeMillis() - this.getStopedTimeMillis();
		this.deltaTime = (this.actualTime - this.lastTime);
		
		Gravitable gravitable;
		Vector2D gravitableVector2d;
		
		for(int i = 0; i < this.gameObjectlist.size(); i++) {
			gravitable = this.gameObjectlist.get(i);
			gravitableVector2d = gravitable.getVelocity();
			
			if(gravitable.isVerticalGravity()) {
				gravitableVector2d.incrementY(this.vector2d.getY() * deltaTime * (float)0.001);
			}
			if(gravitable.isHorizontalGravity()) {
				gravitableVector2d.incrementX(this.vector2d.getX() * deltaTime * (float)0.001);
			}
		}
		
		this.lastTime = this.actualTime;
	}

	public Vector2D getVector2D() {
		return vector2d;
	}

	public void setVector2D(Vector2D vector2d) {
		this.vector2d = vector2d;
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Gravitable;
	}

	@Override
	protected Gravitable getMyInstance(GameObject gameObject) {
		return (Gravitable) gameObject;
	}

}
