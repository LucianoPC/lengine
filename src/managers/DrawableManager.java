package managers;

import interfaces.Drawable;
import interfaces.GameObject;

import java.util.ArrayList;

import android.graphics.Canvas;

public class DrawableManager extends Manager<Drawable> implements Drawable {

	protected Canvas canvas;
	private ArrayList<ArrayList<Drawable>> lines;
	public static final int NUMBER_OF_LINES = 5;

	public DrawableManager() {
		this.canvas = null;
		this.lines = new ArrayList<ArrayList<Drawable>>();

		for (int i = 0; i < NUMBER_OF_LINES; i++) {
			this.lines.add(new ArrayList<Drawable>());
		}
	}

	@Override
	public void addGameObject(GameObject gameObject) {
		super.addGameObject(gameObject);
		if (isMyInstance(gameObject)) {
			this.lines.get(NUMBER_OF_LINES / 2).add((Drawable) gameObject);
		}
	}

	public void setLine(Drawable drawable, int numberLine) {
		int i, j = 0;
		boolean found = false;
		for (i = 0; i < NUMBER_OF_LINES && !found; i++) {
			for (j = 0; j < this.lines.get(i).size(); j++) {
				if (this.lines.get(i).get(j).equals(drawable)) {
					found = true;
					this.lines.get(i).remove(j);
					break;
				}
			}
		}
		if (found) {
			this.lines.get(numberLine).add(drawable);
		}
	}

	@Override
	public void draw(Canvas canvas) {
		this.canvas = canvas;
		update();
	}

	@Override
	public void updateManager() {
		for (int i = 0; i < this.lines.size(); i++) {
			for (int j = 0; j < this.lines.get(i).size(); j++) {
				this.lines.get(i).get(j).draw(this.canvas);
			}
		}
	}

	@Override
	protected boolean isMyInstance(GameObject gameObject) {
		return gameObject instanceof Drawable;
	}

	@Override
	protected Drawable getMyInstance(GameObject gameObject) {
		return (Drawable) gameObject;
	}
}
