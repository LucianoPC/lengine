package interfaces;

import engine_classes.Point2D;
import android.graphics.Rect;

public interface Collisionable extends Steerable {
	
	public static float MAXIMUM_MASS = Float.POSITIVE_INFINITY;
	
	public static final int CIRCLE_POINT_CENTER = 0;
	public static final int CIRCLE_POINT_BORDER = 1;

	public Rect getRect();
	public float getMass();
	public Point2D getCenter();
	public Point2D[] getPoints();
	
}
