package interfaces;

import engine_classes.Point2D;

public interface Positionable extends GameObject {
	
	public Point2D getPosition();
	public int getWidth();
	public int getHeight();
	public float getDegree();
	
}
