package interfaces;

public interface Collision {

	public boolean makeCollision(Collisionable c1, Collisionable c2);
	
}
