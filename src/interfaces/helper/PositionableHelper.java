package interfaces.helper;

import interfaces.Collisionable;
import interfaces.Positionable;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import engine_classes.Point2D;
import engine_classes.Vector2D;

public class PositionableHelper {

	private Paint paint;

	public PositionableHelper() {
		this.paint = new Paint();
		this.paint.setStyle(Style.STROKE);
	}

	public Point2D getCenter(Positionable positionable) {
		Point2D center = new Point2D();
		center.setX(positionable.getPosition().getX()
				+ (positionable.getWidth() / 2));
		center.setY(positionable.getPosition().getY()
				+ (positionable.getHeight() / 2));
		return center;
	}

	public void drawSquarePoints(Canvas canvas, Positionable positionable,
			float degree) {
		Point2D[] points = getSquarePoints(positionable, degree);
		float pts[] = formatPoints(points);

		canvas.drawLines(pts, this.paint);
	}

	public Point2D[] getSquarePoints(Positionable positionable, float degree) {
		Point2D points[];
		RectF rectF = getAABBRectF(positionable);

		points = getPoints(rectF);
		rotatePoints(positionable, points, degree);

		return points;
	}

	public Point2D[] getCirclePoints(Point2D points[], Vector2D direction) {
		Point2D newPoints[] = new Point2D[2];
		float radius = (new Vector2D(points[Collisionable.CIRCLE_POINT_CENTER],
				points[Collisionable.CIRCLE_POINT_BORDER])).getMagnitude();
		Vector2D vectorRadius = new Vector2D(direction);
		vectorRadius.scalar(1.0f / vectorRadius.getMagnitude());
		vectorRadius.scalar(radius);

		newPoints[0] = new Point2D(points[Collisionable.CIRCLE_POINT_CENTER]);
		newPoints[0].sum(vectorRadius);

		vectorRadius.scalar(-1.0f);
		newPoints[1] = new Point2D(points[Collisionable.CIRCLE_POINT_CENTER]);
		newPoints[1].sum(vectorRadius);

		return newPoints;
	}

	public Rect getRect(Positionable positionable, float degree) {
		RectF rectF = getRectF(positionable, degree);
		Rect rect = new Rect((int) rectF.left, (int) rectF.top,
				(int) rectF.right, (int) rectF.bottom);
		return rect;
	}

	public RectF getRectF(Positionable positionable) {
		return getRectF(positionable, 0);
	}

	public RectF getRectF(Positionable positionable, float degree) {
		RectF rectF;

		if (degree % 360 != 0) {
			rectF = getRotatedRect(positionable, degree);
		} else {
			rectF = getAABBRectF(positionable);
		}
		return rectF;
	}

	public RectF getRotatedRect(Positionable positionable, float degree) {
		Point2D points[] = getSquarePoints(positionable, degree);
		RectF rectF = getRectF(points);
		return rectF;
	}

	public RectF getAABBRectF(Positionable positionable) {
		RectF rectF = new RectF(positionable.getPosition().getX(), positionable
				.getPosition().getY(), positionable.getPosition().getX()
				+ positionable.getWidth(), positionable.getPosition().getY()
				+ positionable.getHeight());

		return rectF;
	}

	public void rotatePoints(Positionable positionable, Point2D points[],
			float degree) {
		Point2D pointMid = this.getCenter(positionable);
		rotatePoints(points, pointMid, degree);
	}

	public void rotatePoints(Point2D points[], Point2D pointMid, float degree) {
		for (int i = 0; i < points.length; i++) {
			points[i].rotate(degree, pointMid);
		}
	}

	public Point2D[] getPoints(Rect rect) {
		return getPoints(new RectF(rect));
	}

	public Point2D[] getPoints(RectF rectF) {
		Point2D points[] = new Point2D[4];
		points[0] = new Point2D(rectF.left, rectF.top);
		points[1] = new Point2D(rectF.right, rectF.top);
		points[2] = new Point2D(rectF.right, rectF.bottom);
		points[3] = new Point2D(rectF.left, rectF.bottom);

		return points;
	}

	public RectF getRectF(Point2D points[]) {
		float left = 0, right = 0, top = 0, bottom = 0;

		for (int i = 0; i < points.length; i++) {
			if (points[i].getX() < left || i == 0) {
				left = points[i].getX();
			}
			if (points[i].getX() > right || i == 0) {
				right = points[i].getX();
			}
			if (points[i].getY() < top || i == 0) {
				top = points[i].getY();
			}
			if (points[i].getY() > bottom || i == 0) {
				bottom = points[i].getY();
			}
		}

		return new RectF(left, top, right, bottom);
	}

	public float[] formatPoints(Point2D points[]) {
		float pts[] = new float[points.length * 4];

		for (int i = 0, j = 0; i < points.length; i++, j += 4) {
			pts[j] = points[i].getX();
			pts[j + 1] = points[i].getY();

			pts[j + 2] = points[(i + 1) % points.length].getX();
			pts[j + 3] = points[(i + 1) % points.length].getY();
		}

		return pts;
	}

	public Point2D getCenterOfMass(Point2D points[]) {
		Point2D pointCenterOfMass = new Point2D(centerOfMassX(points),
				centerOfMassY(points));
		return pointCenterOfMass;
	}

	public float centerOfMassX(Point2D points[]) {
		float sumX = 0;
		for (int i = 0; i < points.length; i++) {
			sumX += points[i].getX();
		}
		return sumX / points.length;
	}

	public float centerOfMassY(Point2D points[]) {
		float sumY = 0;
		for (int i = 0; i < points.length; i++) {
			sumY += points[i].getY();
		}
		return sumY / points.length;
	}
	
	public Point2D[] getCirclePoints(Positionable positionable) {
		float centerX, centerY;
		float borderX, borderY;
		Point2D points[] = new Point2D[2];
		for(int i = 0; i < points.length; i++) {
			points[i] = new Point2D();
		}

		centerX = positionable.getPosition().getX() + (positionable.getWidth() / 2);
		centerY = positionable.getPosition().getY() + (positionable.getHeight() / 2);
		points[Collisionable.CIRCLE_POINT_CENTER].setXY(centerX,
				centerY);

		borderX = centerX + (positionable.getWidth() / 2);
		borderY = centerY;
		points[Collisionable.CIRCLE_POINT_BORDER].setXY(borderX,
				borderY);

		return points;
	}
}
