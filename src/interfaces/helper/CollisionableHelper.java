package interfaces.helper;

import interfaces.Collisionable;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;

public class CollisionableHelper extends PositionableHelper {

	private Paint paint;

	public CollisionableHelper() {
		this.paint = new Paint();
		this.paint.setStyle(Style.STROKE);
	}
	
	public void drawPoints(Canvas canvas, Collisionable collisionable) {
		float points[] = formatPoints(collisionable.getPoints());
		canvas.drawLines(points, this.paint);
	}
}
