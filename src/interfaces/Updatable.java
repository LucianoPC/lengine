package interfaces;

public interface Updatable extends GameObject {

	public void update();
}
