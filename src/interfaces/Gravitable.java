package interfaces;

public interface Gravitable extends Steerable{
	
	public boolean isVerticalGravity();
	public boolean isHorizontalGravity();
	
}
