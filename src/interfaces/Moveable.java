package interfaces;

public interface Moveable extends GameObject {

	public boolean isMoving();
	
}
