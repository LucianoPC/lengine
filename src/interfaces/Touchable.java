package interfaces;

import engine_classes.Touch;
import android.view.MotionEvent;

public interface Touchable extends Drawable, Positionable {

	public Touch getTouch();
	public boolean isMyMotionEvent(MotionEvent event);
	public boolean isEnableCallListener();
}
