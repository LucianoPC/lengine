package interfaces;

import android.graphics.Paint;

public interface Paintable {

	public Paint getPaint();
}
