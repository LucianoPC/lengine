package interfaces;

import android.graphics.Canvas;

public interface Drawable extends GameObject {

	public void draw(Canvas canvas);
}
