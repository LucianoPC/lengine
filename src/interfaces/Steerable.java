package interfaces;

import engine_classes.Vector2D;

public interface Steerable extends Positionable, Moveable {

	public Vector2D getVelocity();
	
}
