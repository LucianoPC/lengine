package util;

import android.content.Context;
import android.util.DisplayMetrics;

public class Screen {

	public static int width;
	public static int height;
	public static float density;
	
	public static void initScreen(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		width = displayMetrics.widthPixels;
		height = displayMetrics.heightPixels;
		density = displayMetrics.density;
	}
	
	public static float valueToScreenWidth(float value) {
		return value * width;
	}
	
	public static float valueToScreenHeight(float value) {
		return value * height;
	}

}
