package util;

import interfaces.Updatable;

public class TimeControl implements Updatable{
	
	public interface TimeControlListner{
		public void timeOut(TimeControl timeControl);
	}
	
	private boolean stop;
	private long finalTime;
	private TimeControlListner listner;

	public TimeControl() {
		this.listner = null;
		this.stop = true;
	}
	
	public void waitTime(long time){
		this.finalTime = System.currentTimeMillis() + time;
		this.stop = false;
	}

	@Override
	public void update() {
		if(System.currentTimeMillis() >= this.finalTime && !this.stop){
			this.stop = true;
			if(this.listner != null){
				this.listner.timeOut(this);
			}
		}
	}

	public void setTimeControlListner(TimeControlListner listner) {
		this.listner = listner;
	}
	
	public boolean isStoped() {
		return this.stop;
	}

}
