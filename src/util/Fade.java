package util;

import interfaces.Paintable;
import interfaces.Updatable;

import java.util.ArrayList;

public class Fade implements Updatable {
	
	public interface FadeListener {
		public void onFadeFinish(Fade fade);
	}
	
	private long velocity;
	
	private int finalAlpha;
	private int actualAlpha;
	private int initialAlpha;

	private boolean inFade;
	
	private TimeFunction timeFunction;
	private ArrayList<Paintable> paintableList;
	
	private FadeListener fadeListener;
	
	public Fade() {
		this.timeFunction = new TimeFunction();
		this.paintableList = new ArrayList<Paintable>();
		
		this.inFade = false;
		this.fadeListener = null;
	}
	
	public void addPaintable(Paintable paintable) {
		this.paintableList.add(paintable);
	}
	
	public void removePaintable(Paintable paintable) {
		this.paintableList.remove(paintable);
	}
	
	public void fadeIn(long timeMillis) {
		fadeIn(255, timeMillis);
	}
	
	public void fadeIn(int finalAlpha, long timeMillis) {
		fade(0, finalAlpha, timeMillis);
	}
	
	public void fadeOut(long timeMillis) {
		fadeOut(255, timeMillis);
	}
	
	public void fadeOut(int initialAlpha, long timeMillis) {
		fade(initialAlpha, 0, timeMillis);
	}
	
	public void fade(int initialAlpha, int finalAlpha, long timeMillis) {
		this.initialAlpha = initialAlpha;
		this.finalAlpha = finalAlpha;
		this.actualAlpha = initialAlpha;
		this.velocity = (finalAlpha - initialAlpha) * 1000 / timeMillis;
		
		this.inFade = true;
		this.timeFunction.startTime(0);
		updateAlpha();
	}
	
	@Override
	public void update() {
		if( (this.inFade) && (this.actualAlpha != this.finalAlpha) ) {
			this.actualAlpha = this.initialAlpha + ((int)this.velocity * (int)this.timeFunction.getActualTime() / 1000);
			updateAlpha();
		} else if(this.inFade){
			this.inFade = false;
			
			if(this.fadeListener != null) {
				this.fadeListener.onFadeFinish(this);
			}
		}
	}
	
	public boolean isInFade() {
		return inFade;
	}
	
	public void setFadeListener(FadeListener fadeListener) {
		this.fadeListener = fadeListener;
	}
	
	private void updateAlpha() {
		if( (this.actualAlpha > this.finalAlpha && this.velocity > 0) || 
				(this.actualAlpha < this.finalAlpha && this.velocity < 0) ) {
			this.actualAlpha = this.finalAlpha;
		}
		setAlphaInPaintables(this.actualAlpha);
	}
	
	private void setAlphaInPaintables(int alpha) {
		for(int i = 0; i < this.paintableList.size(); i++) {
			this.paintableList.get(i).getPaint().setAlpha(alpha);
		}
	}

}
