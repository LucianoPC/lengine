package util;

import android.view.MotionEvent;
import engine_classes.Point2D;
import engine_classes.TouchArea;
import engine_classes.Vector2D;

public class Swipe extends TouchArea {

	private Point2D start, end;
	private Vector2D delta;
	private float deltaHorizontal;
	private boolean down;
	
	public Swipe(int width, int height) {
		super(width, height);
		
		this.start = new Point2D();
		this.end = new Point2D();
		this.delta = new Vector2D();
		this.down = false;
	}
	
	@Override
	public boolean isMyMotionEvent(MotionEvent event) {
		if(!isEnable()) {
			return false;
		}
		
		if(event.getAction() == MotionEvent.ACTION_DOWN && this.intersect(event)){
			makeActionDown(event.getX(), event.getY());
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE && this.intersect(event) && !this.down){
			makeActionDown(event.getX(), event.getY());
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_MOVE && this.down){
			makeActionMove(event.getX(), event.getY());			
			return true;
		} else if(event.getAction() == MotionEvent.ACTION_UP && this.down){
			makeActionUp(event.getX(), event.getY());			
			return true;
		}
		return false;
	}
	
	public float getDeltaHorizontal() {
		return deltaHorizontal;
	}
	
	public Point2D getStartPoint() {
		return this.start;
	}
	
	public Point2D getEndPoint() {
		return this.end;
	}
	
	public Point2D getDeltaPoint() {
		return this.delta;
	}
	
	public boolean isDown() {
		return this.down;
	}
	
	protected void makeActionDown(float x, float y) {
		this.setCallListener(false);
		this.start.setXY(x, y);
		this.end.setXY(x, y);
		this.down = true;
	}
	
	protected void makeActionUp(float x, float y) {
		this.setCallListener(true);
		this.end.setXY(x, y);
		this.delta = new Vector2D(start, end);
		this.deltaHorizontal = this.end.getX() - this.start.getX();
		this.down = false;
	}
	
	protected void makeActionMove(float x, float y) {
		this.end.setXY(x, y);
	}
}
