 package util;

import interfaces.Positionable;
import android.content.Context;
import android.util.DisplayMetrics;

public class PositionEditor {
	
	public static final int X = 0;
	public static final int Y = 1;

	private int width;
	private int height;
	
	public PositionEditor() {
		this.width = Screen.width;
		this.height = Screen.height;
	}
	
	public PositionEditor(Context context) {
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		this.width = displayMetrics.widthPixels;
		this.height = displayMetrics.heightPixels;
	}
	
	public void center(Positionable positionable) {
		centerHorizontal(positionable);
		centerVertical(positionable);
	}
	
	public void center(Positionable reference, Positionable positionable) {
		float x = reference.getPosition().getX() + (reference.getWidth()/2) - (positionable.getWidth()/2);
		float y = reference.getPosition().getY() + (reference.getHeight()/2) - (positionable.getHeight()/2);
		positionable.getPosition().setXY(x, y);
	}
	
	public void centerHorizontal(Positionable positionable) {
		float center = (this.width / 2) - (positionable.getWidth() / 2);
		positionable.getPosition().setX(center);
	}
	
	public void centerVertical(Positionable positionable) {
		float center = (this.height / 2) - (positionable.getHeight() / 2);
		positionable.getPosition().setY(center);
	}
	
	public void marginTop(Positionable positionable, float distance) {
		positionable.getPosition().setY(distance);
	}
	
	public void marginTop(Positionable reference, Positionable positionable, float distance) {
		float y = reference.getPosition().getY() - positionable.getHeight() - distance;
		positionable.getPosition().setY(y);
	}
	
	public void allignTop(Positionable reference, Positionable positionable, float distance) {
		float y = reference.getPosition().getY() + distance;
		positionable.getPosition().setY(y);
	}
	
	public void marginBottom(Positionable positionable, float distance) {
		float y = height - positionable.getHeight() - distance;
		positionable.getPosition().setY(y);
	}
	
	public void marginBottom(Positionable reference, Positionable positionable, float distance) {
		float y = reference.getPosition().getY() + reference.getHeight() + distance;
		positionable.getPosition().setY(y);
	}
	
	public void allignBottom(Positionable reference, Positionable positionable, float distance) {
		float y = reference.getPosition().getY() + reference.getHeight() - positionable.getHeight() - distance;
		positionable.getPosition().setY(y);
	}
	
	public void marginRight(Positionable positionable, float distance) {
		float x = width - positionable.getWidth() - distance;
		positionable.getPosition().setX(x);
	}
	
	public void marginRight(Positionable reference, Positionable positionable, float distance) {
		float x = reference.getPosition().getX() + reference.getWidth() + distance;
		positionable.getPosition().setX(x);
	}
	
	public void allignRight(Positionable reference, Positionable positionable, float distance) {
		float x = reference.getPosition().getX() + reference.getWidth() - positionable.getWidth() - distance;
		positionable.getPosition().setX(x);
	}
	
	public void marginLeft(Positionable positionable, float distance) {
		positionable.getPosition().setX(distance);
	}

	public void marginLeft(Positionable reference, Positionable positionable, float distance) {
		float x = reference.getPosition().getX() - positionable.getWidth() - distance;
		positionable.getPosition().setX(x);
	}
	
	public void allignLeft(Positionable reference, Positionable positionable, float distance) {
		float x = reference.getPosition().getX() + distance;
		positionable.getPosition().setX(x);
	}
	
	public void centerOf(Positionable referenceOne, Positionable referenceTwo, Positionable positionable) {
		centerVerticalOf(referenceOne, referenceTwo, positionable);
		centerHorizontalOf(referenceOne, referenceTwo, positionable);
	}
	
	public void centerVerticalOf(Positionable referenceOne, Positionable referenceTwo, Positionable positionable) {
		float centerOne[] = getPositionableCenter(referenceOne);
		float centerTwo[] = getPositionableCenter(referenceTwo);
		float y = (centerOne[Y] + centerTwo[Y]) / 2.0f;
		y -= positionable.getWidth() / 2.0f;
		positionable.getPosition().setY(y);
	}
	
	public void centerHorizontalOf(Positionable referenceOne, Positionable referenceTwo, Positionable positionable) {
		float centerOne[] = getPositionableCenter(referenceOne);
		float centerTwo[] = getPositionableCenter(referenceTwo);
		float x = (centerOne[X] + centerTwo[X]) / 2.0f;
		x -= positionable.getWidth() / 2.0f;
		positionable.getPosition().setX(x);
	}
	
	public float[] getPositionableCenter(Positionable positionable) {
		float center[] = new float[2];
		center[X] = positionable.getPosition().getX() + (positionable.getWidth() / 2);
		center[Y] = positionable.getPosition().getY() + (positionable.getHeight() / 2);
		
		return center;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
}
