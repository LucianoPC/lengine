package util;

public class TimeFunction {

	private long initialTime;

	public TimeFunction() {
		this.initialTime = 0;
	}
	
	public void startTime(long initialTime) {
		this.initialTime = System.currentTimeMillis();
	}

	public long getActualTime() {
		return System.currentTimeMillis() - this.initialTime;
	}
	
}
