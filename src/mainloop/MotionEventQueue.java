package mainloop;

import java.util.ArrayList;

import android.view.MotionEvent;

public class MotionEventQueue {

	private static MotionEventQueue instance;
	
	public static MotionEventQueue getInstance() {
		if(instance == null) {
			instance = new MotionEventQueue();
		}
		return instance;
	}
	
	private ArrayList<MotionEvent> motionEventList;
	
	private MotionEventQueue() {
		this.motionEventList = new ArrayList<MotionEvent>();
	}
	
	public void add(MotionEvent motionEvent) {
		this.motionEventList.add(motionEvent);
	}
	
	public void addList(ArrayList<MotionEvent> motionEventList) {
		for(int i = 0; i < motionEventList.size(); i++) {
			this.motionEventList.add(motionEventList.get(i));
		}
	}

	public MotionEvent getFirst() {
		return this.motionEventList.get(0);
	}

	public void removeFirst() {
		this.motionEventList.remove(0);
	}
	
	public void clear() {
		this.motionEventList.clear();
	}
	
	public boolean isEmpty() {
		return this.motionEventList.isEmpty();
	}
	
}
