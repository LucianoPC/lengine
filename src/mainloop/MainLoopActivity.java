package mainloop;

import java.util.ArrayList;

import util.Constants;
import util.Screen;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Canvas;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.Window;
import android.view.WindowManager;

@SuppressLint("InlinedApi")
public abstract class MainLoopActivity extends Activity {

	private static final boolean SHOW_LOOP_TIME = false;

	private ArrayList<MotionEvent> motionEventList;
	private GameStateMachine gameStateMachine;
	private MotionEventQueue motionEventQueue;
	private MainLoopView mainLoopView;
	private LoopThread loopThread;
	private Boolean stop;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		fullScreen();
		Screen.initScreen(this);

		this.gameStateMachine = GameStateMachine.getInstance();
		this.motionEventList = new ArrayList<MotionEvent>();
		this.motionEventQueue = MotionEventQueue.getInstance();
		this.mainLoopView = new MainLoopView(this);

		setContentView(this.mainLoopView);
		getWindow().setFlags(
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
				WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
		this.mainLoopView.setWillNotDraw(false);
		this.stop = true;

		initialize();

		runMainLoop();
	}

	protected abstract void initialize();

	private void fullScreen() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (this.motionEventList) {
			this.motionEventList.add(event);
		}
		return super.onTouchEvent(event);
	}

	private void runMainLoop() {
		if (this.stop == true) {
			this.stop = false;
			this.loopThread = new LoopThread();

			this.loopThread.start();
		}
	}

	private void stopMainLoop() {
		if (this.loopThread != null) {
			this.stop = true;
			try {
				this.loopThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		runMainLoop();
		this.mainLoopView.resume();

		Log.d(Constants.LOG_DEBUG, "onResume");
	}

	@Override
	protected void onStop() {
		stopMainLoop();
		this.mainLoopView.stop();
		super.onStop();

		Log.d(Constants.LOG_DEBUG, "onStop");
	}

	@Override
	protected void onPause() {
		stopMainLoop();
		this.mainLoopView.stop();
		super.onPause();

		Log.d(Constants.LOG_DEBUG, "onPause");
	}

	@Override
	public void onBackPressed() {
		synchronized (this.mainLoopView) {
			if (!this.mainLoopView.onBackPressed()) {
				if (this.gameStateMachine.size() >= 1) {
					this.gameStateMachine.popState();
				}
			}
		}
		if (this.gameStateMachine.size() < 1) {
			stopMainLoop();
			this.gameStateMachine.clearStates();
			super.onBackPressed();
		}
		Log.d(Constants.LOG_DEBUG, "onBackPressed");
	}

	private class LoopThread extends Thread {

		private long finalTime;
		private long deltaTime;
		private long initialTime;

		private Canvas canvas;
		private SurfaceHolder surfaceHolder;

		@SuppressLint("NewApi")
		@Override
		public void run() {
			surfaceHolder = mainLoopView.getHolder();

			while (!surfaceHolder.getSurface().isValid())
				;
			while (!stop) {
				this.initialTime = System.currentTimeMillis();

				try {
					this.canvas = null;
					this.canvas = this.surfaceHolder.lockCanvas();

					synchronized (this.surfaceHolder) {
						mainLoopView.updateState();
						motionEventQueue.addList(motionEventList);
						motionEventList.clear();
						mainLoopView.update();
						if (this.canvas != null) {
//							canvas.save();
//							canvas.translate(0.0f, Screen.height);
//							canvas.scale(1, -1);
							mainLoopView.drawCanvas(this.canvas);
//							canvas.restore();
						}
					}

				} finally {
					if (this.canvas != null) {
						this.surfaceHolder.unlockCanvasAndPost(this.canvas);
					}
				}

				motionEventQueue.clear();

				this.finalTime = System.currentTimeMillis();

				this.deltaTime = this.finalTime - this.initialTime;

				if (SHOW_LOOP_TIME) {
					if (this.deltaTime > 33) {
						Log.d(Constants.LOG_DEBUG, "deltaTime: "
								+ this.deltaTime);
					}
				}
			}
		}

	}

}
