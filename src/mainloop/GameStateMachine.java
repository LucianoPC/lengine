package mainloop;

import java.util.LinkedList;

public class GameStateMachine {

	private static GameStateMachine instance;
	private LinkedList<GameState> gameStateList;
	
	private GameStateMachine() {
		this.gameStateList = new LinkedList<GameState>();
	}
	
	public static GameStateMachine getInstance() {
		if(instance == null) {
			instance = new GameStateMachine();
		}
		return instance;
	}
	
	public GameState topState() {
		if(!this.gameStateList.isEmpty())
			return this.gameStateList.getFirst();
		else
			return null;
	}
	
	public void addState(GameState gameState) {
		if(this.gameStateList.isEmpty()) {
			this.gameStateList.addFirst(gameState);
		} else if(!gameState.equals(topState())) {
			topState().stopState();
			this.gameStateList.addFirst(gameState);
		}
	}
	
	public void popAddState(GameState gameState) {
		if(!this.gameStateList.isEmpty()) {
			topState().onExit();
			this.gameStateList.removeFirst();
		}
		this.gameStateList.addFirst(gameState);
	}
	
	public void changeState(GameState gameState) {
		clearStates();
		addState(gameState);
	}
	
	public void popState() {
		if(!this.gameStateList.isEmpty()) {
			topState().onExit();
			this.gameStateList.removeFirst();
			
			if(!this.gameStateList.isEmpty()) {
				topState().resumeState();
			}
		}
	}
	
	public void clearStates() {
		while(!this.gameStateList.isEmpty()) {
				topState().onExit();
				this.gameStateList.removeFirst();
		}
	}
	
	public boolean isEmpty() {
		return this.gameStateList.isEmpty();
	}

	public int size() {
		return this.gameStateList.size();
	}
}
