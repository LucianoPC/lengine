package mainloop;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.RelativeLayout;

public class MainLoopView extends RelativeLayout {

	private GameState gameState;
	private GameStateMachine gameStateMachine;
	private MotionEventQueue motionEventQueue;
	
	private MainLoopSurfaceView mainLoopSurfaceView;
	
	public MainLoopView(Context context) {
		super(context);

		this.gameState = null;
		this.gameStateMachine = GameStateMachine.getInstance();
		this.motionEventQueue = MotionEventQueue.getInstance();
		
		this.mainLoopSurfaceView = new MainLoopSurfaceView(context);
		
		addView(this.mainLoopSurfaceView);
	}
	
	public void addMotionEvent(MotionEvent event) {
		this.motionEventQueue.add(event);
	}
	
	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}
	
	public SurfaceHolder getHolder() {
		return this.mainLoopSurfaceView.getHolder();
	}
	
	public void updateState() {
		GameState topState = this.gameStateMachine.topState();
		if( (!(topState == null)) &&
			(!topState.equals(this.gameState)) ) {
			this.gameState = this.gameStateMachine.topState();
			this.gameState.startState();
		}
	}
	
	public void resume() {
		if(this.gameState != null) {
			this.gameState.resumeState();
		}
	}
	
	public void stop() {
		if(this.gameState != null) {
			this.gameState.stopState();
		}
	}
	
	public void drawCanvas(Canvas canvas) {
		if(this.gameState != null) {
			this.gameState.drawCanvas(canvas);
		}
	}
	
	public void update() {
		if(this.gameState != null) {
			this.gameState.update();
		}
	}
	
	public boolean onBackPressed() {
		if(this.gameState != null) {
			return this.gameState.onBackPressed();
		}
		return false;
	}
	
	public class MainLoopSurfaceView extends SurfaceView implements SurfaceHolder.Callback {

		public MainLoopSurfaceView(Context context) {
			super(context);
			getHolder().addCallback(this);
		}

		@Override
		public void surfaceChanged(SurfaceHolder holder, int format, int width,
				int height) {
			
		}
	
		@Override
		public void surfaceCreated(SurfaceHolder holder) {
			
		}
	
		@Override
		public void surfaceDestroyed(SurfaceHolder holder) {
			
		}
	}
}
