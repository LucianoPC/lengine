package mainloop;

import interfaces.GameObject;

import java.util.ArrayList;
import java.util.List;

import managers.DrawableManager;
import managers.Manager;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;

public abstract class GameState {

	private DrawableManager drawableManager;
	private ArrayList<Manager<?>> managerList;

	private Context context;
	private Resources resources;

	private Paint statePaint;
	private Rect rectBackground;

	private int id;
	private boolean initialized;
	private boolean enableBackgroundColor;

	public GameState(int id, Context context, Resources resources) {
		this.id = id;
		this.context = context;
		this.resources = resources;

		this.initialized = false;
		this.enableBackgroundColor = true;

		this.managerList = new ArrayList<Manager<?>>();
		this.drawableManager = new DrawableManager();

		this.statePaint = new Paint();
		DisplayMetrics displayMetrics = context.getResources()
				.getDisplayMetrics();
		this.rectBackground = new Rect(0, 0, displayMetrics.widthPixels,
				displayMetrics.heightPixels);
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof GameState)) {
			return false;
		}
		GameState otherGameState = (GameState) object;

		return this.id == otherGameState.id;
	}

	public void addGameObject(GameObject gameObject) {
		for (int i = 0; i < this.managerList.size(); i++) {
			this.managerList.get(i).addGameObject(gameObject);
		}
		this.drawableManager.addGameObject(gameObject);
	}

	public void addGameObjectList(List<GameObject> gameObjectList) {
		for (GameObject gameObject : gameObjectList) {
			addGameObject(gameObject);
		}
	}

	public void removeGameObject(GameObject gameObject) {
		for (int i = 0; i < this.managerList.size(); i++) {
			this.managerList.get(i).removeGameObject(gameObject);
		}
		this.drawableManager.removeGameObject(gameObject);
	}

	public void removeGameObjectList(List<GameObject> gameObjectList) {
		for (GameObject gameObject : gameObjectList) {
			removeGameObject(gameObject);
		}
	}

	public void addManager(Manager<?> manager) {
		this.managerList.add(manager);
	}

	public void removeManager(Manager<?> manager) {
		this.managerList.remove(manager);
	}

	public void update() {
		for (int i = 0; i < this.managerList.size(); i++) {
			this.managerList.get(i).update();
		}
		updateGameState();
	}

	public void drawCanvas(Canvas canvas) {
		if (this.enableBackgroundColor) {
			canvas.drawRect(this.rectBackground, this.statePaint);
		}
		this.drawableManager.draw(canvas);
	}

	public boolean onBackPressed() {
		return false;
	}

	public void startState() {
		if (!initialized) {
			this.onInit();
			this.initialized = true;
		} else {
			this.resumeState();
		}
	}

	public void updateGameState() {
	}

	protected abstract void onInit();

	protected abstract void onResume();

	protected abstract void onExit();

	public void resumeState() {
		for (Manager<?> manager : managerList) {
			manager.setEnable(true);
		}
		onResume();
	}

	public void stopState() {
		for (Manager<?> manager : managerList) {
			manager.setEnable(false);
		}
		onExit();
	}

	public void setDrawableManager(DrawableManager drawableManager) {
		this.drawableManager = drawableManager;
	}

	public Context getContext() {
		return context;
	}

	public Resources getResources() {
		return resources;
	}

	public int getStateBackground() {
		return this.statePaint.getColor();
	}

	public void setStateBackground(int stateBackground) {
		this.statePaint.setColor(stateBackground);
	}

	public int getId() {
		return id;
	}

	public boolean isEnableBackgroundColor() {
		return enableBackgroundColor;
	}

	public void setEnableBackgroundColor(boolean enableBackgroundColor) {
		this.enableBackgroundColor = enableBackgroundColor;
	}
	
	public DrawableManager getDrawableManager(){
		return this.drawableManager;
	}
}
